<?php
return [
    'upload_path'          => date('Ymd').'/',   //文件基于根目录的目录规则
    'upload_driver'        => 'local',          //使用的上传驱动
    'upload_size'          => '10', //MB
    'upload_exts'          => 'jpeg,jpg,gif,bmp,png,doc,xls,mp4,m4a,mp3,aac',
    'upload_relative'      => 'http://www.sapixx.com/', //访问域名
    'upload_driver_config' => [
        'local' => [
            'driver' => 'local'
        ],
        /* 城市名称：
        *  经典网络下可选：杭州、上海、青岛、北京、张家口、深圳、香港、硅谷、弗吉尼亚、新加坡、悉尼、日本、法兰克福、迪拜
        *  VPC 网络下可选：杭州、上海、青岛、北京、张家口、深圳、硅谷、弗吉尼亚、新加坡、悉尼、日本、法兰克福、迪拜
        */
        'oss' => [
            'driver'      => 'oss',
            'city'        => '张家口',
            'networkType' => '经典网络',  //经典网络 or VPC
            'access_id'   => '',
            'secret_key'  => '',
            'bucket'      => '',
            'isInternal'  => true,
            'domain'      => '', //必须 / 结尾
            'internal'    => '' //必须 / 结尾
        ]
    ],
];