/*
Navicat MySQL Data Transfer

Source Server         : 本地开发
Source Server Version : 50721
Source Host           : localhost:3306
Source Database       : wechat

Target Server Type    : MYSQL
Target Server Version : 50721
File Encoding         : 65001

Date: 2020-03-11 14:53:07
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ai_config_apis
-- ----------------------------
CREATE TABLE `ai_config_apis` (
  `id` tinyint(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `apikey` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='阿里API市场接口配置';

-- ----------------------------
-- Table structure for ai_config_user
-- ----------------------------
CREATE TABLE `ai_config_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `locks` tinyint(1) DEFAULT '0',
  `username` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `about` tinytext,
  `last_login_time` int(12) unsigned NOT NULL DEFAULT '0',
  `last_login_ip` varchar(15) DEFAULT '0',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='管理员';

-- ----------------------------
-- Records of ai_config_user
-- ----------------------------
INSERT INTO `ai_config_user` VALUES ('1', '0', 'sapixx', '$2y$10$0jvxTZaCOdODeJcyrVLSw.EANfX0rPFReMjmCLJOomdtSoggDXpBm', '管理员', '1583138081', '127.0.0.1', '1516258124', '1574394237');

-- ----------------------------
-- Table structure for ai_config_web
-- ----------------------------
CREATE TABLE `ai_config_web` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `keywords` varchar(200) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `icp` varchar(50) DEFAULT NULL,
  `contacts` varchar(50) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='站点管理';


-- ----------------------------
-- Table structure for ai_member
-- ----------------------------
CREATE TABLE `ai_member` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `bind_member_miniapp_id` int(11) DEFAULT '0',
  `open_id` varchar(250) DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT '0',
  `phone_id` bigint(15) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `safe_password` varchar(255) DEFAULT NULL,
  `is_lock` int(11) DEFAULT '0',
  `lock_config` tinyint(2) DEFAULT '0',
  `login_ip` varchar(20) DEFAULT NULL,
  `login_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `ticket` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='小程序成员管理';

-- ----------------------------
-- Table structure for ai_member_bank
-- ----------------------------
CREATE TABLE `ai_member_bank` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `member_id` int(20) DEFAULT '0',
  `money` decimal(10,2) DEFAULT '0.00' COMMENT '帐号余额',
  `lack_money` decimal(10,2) DEFAULT '0.00' COMMENT '锁定金额',
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='个人资金';

-- ----------------------------
-- Table structure for ai_member_bank_bill
-- ----------------------------
CREATE TABLE `ai_member_bank_bill` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `money` decimal(10,2) DEFAULT '0.00' COMMENT '分',
  `member_id` int(11) DEFAULT '0',
  `state` tinyint(1) DEFAULT '0',
  `message` tinytext,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `id_2` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='财务变动记录';

-- ----------------------------
-- Table structure for ai_member_bank_recharge
-- ----------------------------
CREATE TABLE `ai_member_bank_recharge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `order_sn` varchar(255) DEFAULT NULL,
  `money` decimal(10,2) DEFAULT NULL,
  `state` tinyint(1) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `transaction_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_member_form
-- ----------------------------
CREATE TABLE `ai_member_form` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `form_id` varchar(100) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `is_del` tinyint(2) DEFAULT '0',
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='小程序模板消息ID';

-- ----------------------------
-- Table structure for ai_member_keyword
-- ----------------------------
CREATE TABLE `ai_member_keyword` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `is_miniapp` tinyint(1) DEFAULT '0',
  `type` varchar(50) DEFAULT '',
  `keyword` varchar(100) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `content` text,
  `media_id` varchar(255) DEFAULT NULL,
  `media` text,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='微信公众号关键字服务';

-- ----------------------------
-- Table structure for ai_member_miniapp
-- ----------------------------
CREATE TABLE `ai_member_miniapp` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT '0' COMMENT '前台管理员ID',
  `service_id` varchar(200) DEFAULT NULL,
  `miniapp_order_id` int(11) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `miniapp_id` int(11) DEFAULT '0',
  `appname` varchar(100) DEFAULT NULL,
  `head_img` varchar(255) DEFAULT NULL,
  `qrcode_url` varchar(255) DEFAULT NULL,
  `miniapp_appid` char(50) DEFAULT NULL,
  `miniapp_secret` varchar(255) DEFAULT NULL,
  `miniapp_head_img` varchar(255) DEFAULT NULL,
  `miniapp_qrcode_url` varchar(255) DEFAULT NULL,
  `navbar_color` varchar(50) DEFAULT NULL,
  `navbar_style` varchar(50) DEFAULT NULL,
  `is_lock` tinyint(1) DEFAULT '0' COMMENT '0正常 1锁定',
  `is_open` tinyint(1) DEFAULT '0' COMMENT '0自助应用 1开放平台应用',
  `is_psp` tinyint(1) DEFAULT NULL,
  `psp_appid` varchar(32) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `service_time` int(11) DEFAULT '0',
  `mp_appid` varchar(30) DEFAULT NULL,
  `mp_head_img` varchar(255) DEFAULT NULL,
  `mp_qrcode_url` varchar(255) DEFAULT NULL,
  `mp_secret` varchar(50) DEFAULT NULL,
  `mp_token` varchar(50) DEFAULT NULL,
  `mp_aes_key` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_member_miniapp_code
-- ----------------------------
CREATE TABLE `ai_member_miniapp_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `is_commit` tinyint(1) DEFAULT NULL COMMENT '0上传2已提交3审核',
  `trial_qrcode` varchar(255) DEFAULT NULL,
  `auditid` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_member_miniapp_order
-- ----------------------------
CREATE TABLE `ai_member_miniapp_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_lock` tinyint(1) DEFAULT '0',
  `member_id` int(11) NOT NULL,
  `miniapp_id` int(11) NOT NULL,
  `update_var` int(11) DEFAULT NULL,
  `start_time` int(11) NOT NULL,
  `end_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_member_miniapp_token
-- ----------------------------
CREATE TABLE `ai_member_miniapp_token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `authorizer_appid` varchar(255) DEFAULT NULL,
  `authorizer_access_token` varchar(255) DEFAULT NULL,
  `authorizer_refresh_token` varchar(255) DEFAULT NULL,
  `expires_in` varchar(255) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_member_official_menu
-- ----------------------------
CREATE TABLE `ai_member_official_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `types` varchar(20) DEFAULT NULL,
  `parent_id` int(11) DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `sort` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `pagepath` varchar(255) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='公众号菜单关联';

-- ----------------------------
-- Table structure for ai_member_payment
-- ----------------------------
CREATE TABLE `ai_member_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `apiname` char(20) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `config` text,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_member_sms
-- ----------------------------
CREATE TABLE `ai_member_sms` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `is_new` tinyint(1) DEFAULT NULL,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `message` text,
  `create_time` int(11) DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='站内信';

-- ----------------------------
-- Table structure for ai_member_sms_queue
-- ----------------------------
CREATE TABLE `ai_member_sms_queue` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(11) DEFAULT NULL,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `is_send` tinyint(2) DEFAULT NULL,
  `param` text,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ISSEND` (`is_send`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='站内信';

-- ----------------------------
-- Table structure for ai_member_subscribe_queue
-- ----------------------------
CREATE TABLE `ai_member_subscribe_queue` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(11) DEFAULT NULL,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `is_send` tinyint(2) DEFAULT NULL,
  `param` text,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ISSEND` (`is_send`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订阅消息';

-- ----------------------------
-- Table structure for ai_member_wechat_tpl
-- ----------------------------
CREATE TABLE `ai_member_wechat_tpl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `tplmsg_common_app` varchar(255) DEFAULT NULL,
  `tplmsg_common_wechat` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for ai_user
-- ----------------------------
CREATE TABLE `ai_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `invite_code` varchar(50) DEFAULT NULL,
  `member_miniapp_id` int(11) DEFAULT '0',
  `password` varchar(255) DEFAULT NULL,
  `phone_uid` varchar(20) DEFAULT NULL,
  `wechat_uid` varchar(255) DEFAULT NULL,
  `miniapp_uid` varchar(255) DEFAULT NULL,
  `official_uid` varchar(255) DEFAULT NULL,
  `session_key` varchar(255) DEFAULT NULL,
  `safe_password` varchar(255) DEFAULT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `face` varchar(255) DEFAULT NULL,
  `login_ip` varchar(20) DEFAULT NULL,
  `login_time` int(11) DEFAULT NULL,
  `is_lock` tinyint(1) DEFAULT '0',
  `is_delete` tinyint(1) DEFAULT '0',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `CODE` (`invite_code`),
  KEY `MINIAPP_ID` (`member_miniapp_id`),
  KEY `WECHAT_ID` (`miniapp_uid`),
  KEY `OPEN_ID` (`official_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户基础ID';

-- ----------------------------
-- Table structure for ai_user_address
-- ----------------------------
CREATE TABLE `ai_user_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `is_first` tinyint(1) NOT NULL DEFAULT '0',
  `address` varchar(255) DEFAULT NULL,
  `telphone` varchar(50) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_user_bank
-- ----------------------------
CREATE TABLE `ai_user_bank` (
  `member_miniapp_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `idcard` varchar(20) DEFAULT NULL,
  `bankname` varchar(255) DEFAULT NULL,
  `bankid` varchar(50) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `UID` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='银行账号';

-- ----------------------------
-- Table structure for ai_user_level
-- ----------------------------
CREATE TABLE `ai_user_level` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  `parent_id` bigint(20) NOT NULL DEFAULT '0',
  `level` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `level` (`level`),
  KEY `parent_id` (`parent_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户关系表';

-- ----------------------------
-- Table structure for ai_miniapp
-- ----------------------------
CREATE TABLE `ai_miniapp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `view_pic` varchar(255) DEFAULT NULL,
  `style_pic` text,
  `version` varchar(255) DEFAULT NULL,
  `expire_day` varchar(255) DEFAULT NULL,
  `sell_price` decimal(10,2) DEFAULT NULL,
  `market_price` decimal(10,2) DEFAULT NULL,
  `miniapp_dir` varchar(255) DEFAULT NULL,
  `describe` text,
  `content` text,
  `qrcode` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT '0',
  `types` char(20) DEFAULT NULL,
  `is_lock` tinyint(1) DEFAULT '0',
  `is_manage` tinyint(1) DEFAULT '0' COMMENT '平台管理',
  `is_wechat_pay` tinyint(1) DEFAULT '0' COMMENT '0关闭支付1开启支付',
  `is_alipay_pay` tinyint(1) DEFAULT NULL,
  `is_openapp` tinyint(1) DEFAULT NULL,
  `template_id` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_member_cloud
-- ----------------------------
CREATE TABLE `ai_member_cloud` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `unionId` varchar(100) DEFAULT NULL,
  `openId` varchar(100) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_member_cloud_product
-- ----------------------------

CREATE TABLE `ai_member_cloud_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `miniapp_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET FOREIGN_KEY_CHECKS=1;