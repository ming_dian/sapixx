<?php
return [
    [
        'name' => '应用管理',
        'icon' => 'my_icon',
        'menu' => [
            ['name' => '客户应用','url' => url('system/admin.miniapp/authorizar'),'icon' => 'yingyongyuanma'],
            ['name' => '用户管理','url' => url('system/admin.member/index'),'icon' => 'my_icon'],
            ['name' => '应用管理','url' => url('system/admin.miniapp/index'),'icon' => 'setup_icon'],
        ]
    ],
    [
        'name' => '微信服务',
        'icon' => 'weixin',
        'menu' => [
            ['name' => '开放平台', 'url' => url('system/admin.setting/wechatOpen')],
            ['name' => '微信支付', 'url' => url('system/admin.setting/wechatPay')],
            ['name' => '服务号', 'url' => url('system/admin.setting/wechatAccount')],
            ['name' => '腾讯云市场','url' => url('system/admin.memberCloud/index')],
        ]
    ],
    [
        'name' => '阿里服务',
        'icon' => 'zhifubao',
        'menu' => [
            ['name' => '短信服务','url' => url('system/admin.setting/aliSms')],
            ['name' => '阿里云市场','url' => url('system/admin.setting/aliApi')]
        ]
    ],
    [
        'name' => '系统配置',
        'icon' => 'tools-hardware',
        'menu' => [
            ['name' => '管理员','url' => url('system/admin.user/index')],
            ['name' => '站点管理','url' => url('system/admin.setting/webConfig')],
        ]
    ]
];
