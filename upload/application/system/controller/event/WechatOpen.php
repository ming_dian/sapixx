<?php
/**
 * @copyright   Copyright (c) 2017 https://www.sapixx.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @author      pillar<ltmn@qq.com>
 * 微信第三方开放平台开放-接入
 */
namespace app\system\controller\event;
use think\Controller;
use app\common\event\User;
use app\common\model\User as ModelUser;
use app\common\facade\Inform;
use app\common\facade\WechatMp;
use app\common\facade\WechatProgram;
use app\common\model\MemberMiniapp;
use app\common\model\MemberKeyword;
use EasyWeChat\OpenPlatform\Server\Guard;
use EasyWeChat\Kernel\Messages\Text;
use EasyWeChat\Kernel\Messages\Image;
use EasyWeChat\Kernel\Messages\News;
use EasyWeChat\Kernel\Messages\NewsItem;
use EasyWeChat\Kernel\Messages\Media;
use app\allwin\model\AllwinEcardUser;
use Exception;

class WechatOpen extends Controller{

    /**
     * 微信开放平台推送车票(1次/10分钟)
     * 有了车票要保存下来,获取授权时要用
     * @return json
     */
    public function ticket(){
        try {
            $server = WechatMp::openConfig()->server;
            // 处理授权成功事件
            $server->push(function ($message) {

            }, Guard::EVENT_AUTHORIZED);
            // 处理授权更新事件
            $server->push(function ($message) {

            }, Guard::EVENT_UPDATE_AUTHORIZED);
            // 处理授权取消事件
            $server->push(function ($message) {

            }, Guard::EVENT_UNAUTHORIZED);
            $server->serve();
            return response("success");
        }catch (Exception $e) {
            $this->error($e->getMessage());
        }
    }

    /**
     * 微信开放平台事件接受
     * @return json
     */
    public function message($appid){
        try {
            //公众号和小程序开放平台接入验证
            if ($appid == 'wx570bc396a51b8ff8' || $appid == 'wxd101a85aa106f53e') {
                $app = $appid == 'wx570bc396a51b8ff8' ? WechatMp::openConfig()->officialAccount($appid) : WechatMp::openConfig()->miniProgram($appid);
                $app->server->push(function ($message) {
                    switch ($message['MsgType']) {
                        case 'event':
                            return $message['Event'].'from_callback';
                            break;
                        case 'text':
                            if ($message['Content'] == "TESTCOMPONENT_MSG_TYPE_TEXT") {
                                return 'TESTCOMPONENT_MSG_TYPE_TEXT_callback';
                            } else {
                                $authCode = explode(":",$message['Content'])[1];
                                return new Text($authCode."_from_api");
                            }
                            break;
                        default;
                            return '平台验证';
                            break;
                    }
                });
            } else {
                $miniapp = MemberMiniapp::whereOr(['miniapp_appid' => $appid,'mp_appid' => $appid])->field('id,miniapp_appid,mp_appid')->find();
                if(empty($miniapp)){
                    return response("未找到应用");
                }
                $member_miniapp_id = $miniapp->id;
                $is_miniapp        = 0;
                if($miniapp->mp_appid == $appid){
                    $app = WechatMp::isTypes($member_miniapp_id);
                }else{
                    $app = WechatProgram::isTypes($member_miniapp_id);
                    $is_miniapp = 1;
                }
                $app->server->push(function($message) use ($member_miniapp_id,$is_miniapp){
                    if($message['MsgType'] == 'event'){
                        $keyword = isset($message['EventKey']) ? $message['EventKey'] : $message['Event'];
                    }else{
                        $keyword = $message['Content'];
                    }
                    switch ($keyword) {
                        case 'user_del_card': //用户删除会员卡
                            AllwinEcardUser::where(['code' => $message['UserCardCode']])->update(['is_del' => 1]);
                            break;
                        case 'card_sku_remind': //会员卡库存不足100
                            //后台管理员微信通知
                            Inform::sms(User::isFounder($member_miniapp_id)->user_id,$member_miniapp_id,['title' =>'业务进展通知','type' => '会员卡','content' =>'会员卡库存不足，请尽快补充']);
                            break;
                        case 'user_get_card': //用户领取会员卡
                            if($message['IsRestoreMemberCard'] == 1){ //用户删除后找回卡片
                                AllwinEcardUser::where(['code' => $message['UserCardCode']])->update(['is_del' => 0]);
                            }else{ //用户领取新卡片
                                $info = ModelUser::where(['official_uid' => $message['FromUserName']])->find();
                                $ecardUser = AllwinEcardUser::where(['member_miniapp_id' => $member_miniapp_id,'uid' => $info['id'],'card_id' => $message['CardId'],'is_del' => 0])->find();
                                if($ecardUser){
                                    return;
                                }
                                AllwinEcardUser::create(['member_miniapp_id' => $member_miniapp_id,'card_id' => $message['CardId'],'code' => $message['UserCardCode'],'uid' => $info['id'],'outer_str' => isset($message['OuterStr']) ? $message['OuterStr'] : '','create_time' => time()]);
                            }
                            return;
                            break;
                        case 'card_pass_check'://会员卡通过审核
                            //后台管理员微信通知
                            Inform::sms(User::isFounder($member_miniapp_id)->user_id,$member_miniapp_id,['title' =>'业务进展通知','type' => '会员卡','content' =>'会员卡已过审核']);
                            break;
                        case 'card_not_pass_check'://会员卡未通过审核
                            //后台管理员微信通知
                            Inform::sms(User::isFounder($member_miniapp_id)->user_id,$member_miniapp_id,['title' =>'业务进展通知','type' => '会员卡','content' =>'会员卡未通过审核']);
                            break;
                        default:
                            $rel = MemberKeyword::where(['member_miniapp_id' => $member_miniapp_id,'is_miniapp' => $is_miniapp,'keyword' => $keyword])->find();
                            if(empty($rel)){
                                return;
                            }
                            switch ($rel->type) {
                                case 'link':
                                    $item = ['title' => $rel->title,'description' => $rel->content,'url' => $rel->url];
                                    if($is_miniapp){
                                        $item['thumb_url'] = $rel->image;
                                    }else{
                                        $item['image'] = $rel->image;
                                    }
                                    $items = [new NewsItem($item)];
                                    $msg = new News($items);
                                    break;
                                case 'image':
                                    $msg = new Image($rel->media_id);
                                    break;
                                case 'media':
                                    $msg = new Media($rel->media_id,'mpnews'); //mpnews、 mpvideo、voice、image
                                    break;
                                default:
                                    $msg = new Text($rel->content);
                                    break;
                            }
                            if($is_miniapp){
                                WechatProgram::isTypes($member_miniapp_id)->customer_service->message($msg)->to($message['FromUserName'])->send();
                            }else{
                                return $msg;
                            }
                            break;
                    }
                });
            }
            $app->server->serve()->send();
        }catch (Exception $e) {
            $this->error($e->getMessage());
        }
    }
}