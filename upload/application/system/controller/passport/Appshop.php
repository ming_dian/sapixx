<?php
/**
 * @copyright   Copyright (c) 2017 https://www.sapixx.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @author      pillar<ltmn@qq.com>
 * 应用管理
 */
namespace app\system\controller\passport;
use app\common\event\Passport;
use app\common\model\Member;
use app\common\model\MemberMiniapp;
use app\common\model\Miniapp;
use app\common\model\MemberMiniappOrder;
use app\system\model\MemberBank;
use app\system\model\MemberBankBill;

class Appshop extends Common{

    public function initialize(){
        parent::initialize();
        if($this->user->parent_id){
            $this->error('仅创始人有权限访问');
        }
        $this->assign('pathMaps', [['name'=>'应用商店','url'=>url('system/passport.appshop/index')]]);
    }

   /* 列表
    * @access public
    */
   public function index(){
       $view['list']  = Miniapp::where(['is_lock' => 0])->order('id desc')->paginate(10,true);
       return view()->assign($view);
   }
   
   /**
    * 阅读内容
    * @return void
    */
   public function review(int $id){
        $view['info']  = Miniapp::where(['id' => $id,'is_lock' => 0])->find();
        if(!$view['info']){
            return $this->error("404 NOT FOUND");
        }
        $view['style_pic'] =  empty($view['info']['style_pic']) ? [] :json_decode($view['info']['style_pic'],true);
        $view['miniapp']   =  MemberMiniappOrder::where(['member_id' => $this->user['id'],'miniapp_id' => $id])->count(); 
        $view['pathMaps'] =  [['name'=>'应用商店','url'=>url('system/passport.appshop/index')],['name'=>$view['info']['title'],'url'=>url('passport.appshop/review',['id' => $id])]];
        return view()->assign($view);
   }

   /**
    * 购买小程序
    */
   public function buy($input){
        if(request()->isPost()){
            $data = [
                'member_id'    => $this->user['id'],
                'id'           => $this->request->param('miniapp_id/d'),
                'title'        => $this->request->param('appname/s'),
                'safepassword' => $this->request->param('safepassword/s'),
            ];
            $validate = $this->validate($data,'MemberBank.buy');
            if(true !== $validate){
                return json(['code'=>0,'msg'=>$validate]);
            }
            //判断安全密码是否正确
            $isPass = empty($this->user->safepassword) ? Member::checkPasspord($this->user->id,$data['safepassword']) : Member::checkSafePasspord($this->user->id,$data['safepassword']);
            if(!$isPass){
                return enjson(0,'密码不正确');
            }
            $miniapp  = Miniapp::where(['id' => $data['id'],'is_lock' => 0])->field('id,sell_price,template_id')->find();
            if(empty($miniapp)){
                return json(['code'=>0,'msg'=>'未找到应用']);
            }
            //如果价格<=0，就不在查询数据库
            if($miniapp->sell_price > 0){
                $rel = MemberBank::moneyJudge($this->user->id,$miniapp->sell_price);
                if($rel){
                    return json(['code'=>0,'msg'=>'余额不足,请充值']);
                }
            }
            //新增购买列表
            $order['member_id']  = (int)$data['member_id'];
            $order['miniapp_id'] = (int)$data['id'];
            $order['update_var'] = (int)$miniapp->template_id;
            $order['start_time'] = time();
            $order['end_time']   = time() + 31536000;
            $id = MemberMiniappOrder::insertGetId($order);
            MemberBank::moneyUpdate($this->user->id,-$miniapp->sell_price);
            MemberBankBill::create(['state' => 1,'money' => $miniapp->sell_price,'member_id' => $this->user['id'],'message' => '购买应用程序' . $data['title'],'update_time' => time()]);
            $member_miniapp_id = MemberMiniapp::insertGetId(['miniapp_order_id' => $id,'member_id' => $this->user['id'],'miniapp_id' => $data['id'],'appname' => $data['title'],'create_time' => time()]);
            MemberMiniapp::where(['id' => $member_miniapp_id])->update(['service_id' => uuid(3,true,$member_miniapp_id)]); //生成应用服务ID
            Passport::setMiniapp(['member_id' => $this->user['id'],'miniapp_id' => $miniapp->id,'member_miniapp_id' => $member_miniapp_id]);  //设置登录
            return json(['code'=>200,'msg'=>'开通成功,授权你的公众号或小程序即可开始使用','parent' => 1]);
        }else{
            $view['info']  = Miniapp::where(['id' => $input,'is_lock' => 0])->find();
            if(!$view['info']){
                return $this->error("404 NOT FOUND");
            }
            $view['bank']    = MemberBank::where(['member_id' => $this->user['id']])->find();
            $view['miniapp'] = MemberMiniappOrder::where(['member_id' => $this->user['id'],'miniapp_id' => $input])->count(); 
            return view()->assign($view);
        }
   }
}