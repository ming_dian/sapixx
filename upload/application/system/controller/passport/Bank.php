<?php
/**
 * @copyright   Copyright (c) 2017 https://www.sapixx.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @author      pillar<ltmn@qq.com>
 * 我的余额
 */
namespace app\system\controller\passport;
use app\common\model\MemberMiniapp;
use app\common\model\ConfigApis;
use app\common\facade\WechatPay;
use app\system\model\MemberBank;
use app\system\model\MemberBankBill;
use app\system\model\MemberBankRecharge;
use think\facade\Request;

class Bank extends Common{

    public function initialize() {
        parent::initialize();
        if($this->user->parent_id){
            $this->error('无权限访问,只有创始人身份才允许使用。');
        }
    }

    /**
     * 我的应用
     * @access public
     */
    public function index(){
        $view['list'] = MemberMiniapp::where(['member_id' => $this->user->id])->order('create_time desc')->paginate(10,false);
        $view['pathMaps'] = [['name'=>'财务管理','url'=>'javascript:;'],['name'=>'我的应用','url'=>'javascript:;']];   
        return view()->assign($view);
    }

    /**
     * @return \think\response\View
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * 我的帐单
     */
    public function bill(){
        $view['bank'] = MemberBank::where(['member_id' => $this->user->id])->find();
        $view['consume'] = MemberBankBill::where(['member_id' => $this->user->id,'state' => 1])->sum('money');
        $view['list'] = MemberBankBill::where(['member_id' => $this->user->id])->order('update_time desc')->paginate(20,false);
        $view['pathMaps'] = [['name'=>'财务管理','url'=>'javascript:;'],['name'=>'我的帐单','url'=>'javascript:;']];
        return view()->assign($view);
    }

    /**
     * 账户充值
     * @return \think\response\View
     */
    public function pay(){
        if(request()->isAjax()) {
            $payType = Request::param('payType/d');
            $money   = Request::param('money/d');
            if(empty($money) || $money < 10){
                return enjson(204, '最小大于10元');
            }
            $data = [
                'money'     => $money,
                'member_id' => $this->user->id,
                'order_sn'  => order_no(),
            ];
            $rel = MemberBankRecharge::order($data);
            if ($rel) {
                if($payType == 0){
                    $config = ConfigApis::Config('wepay');
                    $order = [
                        'trade_type'   => 'NATIVE',
                        'body'         => '充值',
                        'out_trade_no' => $data['order_sn'],
                        'total_fee'    => $data['money'] * 100,   //分
                        'mch_id'       => $config['mch_id'],
                        'appid'        => $config['app_id'],
                        'notify_url'   => api(1,'system/notify/index'),
                    ];
                    $result = WechatPay::doPay()->order->unify($order);
                    if($result['return_code'] == 'SUCCESS'){
                        if($result['result_code'] == 'SUCCESS'){
                            return enjson(200,'操作成功',$result);
                        }else{
                            return enjson(0,$result['return_msg']);
                        }
                    }else{
                        return enjson(0,$result['return_msg']);
                    }
                }elseif($payType == 1){
                    //支付宝
                }else{
                    return enjson(0);
                }
            } else {
                return enjson(0);
            }
        }else{
            $view['pathMaps'] = [['name'=>'财务管理','url'=>'javascript:;'],['name'=>'账户充值','url'=>'javascript:;']];   
            return view()->assign($view);
        }
    }
}