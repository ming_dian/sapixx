<?php
/**
 * @copyright   Copyright (c) 2017 https://www.sapixx.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @author      pillar<ltmn@qq.com>
 * 后台首页
 */
namespace app\system\controller\admin;
use filter\Filter;
use app\common\model\Miniapp;
use app\common\model\ConfigUser;
use app\system\event\AppConfig;
use app\common\event\Admin;

class Index extends Common{

    protected $login;

    public function initialize() {
        parent::initialize();
        $this->login = Admin::getLoginSession();
    }

    /**
     * 后台主框架
     */
    public function index(){
        $miniapp = Admin::getMiniapp();
        $ary = [];
        if($miniapp ){
            $menu = Miniapp::where(['is_manage' => 1,'id' => $miniapp['miniapp_id']])->find();
            if(!empty($menu)){
                $ary['name']   = $menu['title'];
                $ary['wechat'] = $menu['miniapp_dir'];
            }
        }
        $view['miniapp'] = $ary;
        return view('admin/index/index')->assign($view);
    }

    /**
     * 管理菜单
     * @return json
     */
    public function appmenu($app = 'system'){
        return json(AppConfig::menu(Filter::filter_escape($app),false));  
    }
    /**
     * 后台登录
     */
    public function login(){
        if(request()->isPost()){
            $data = [
                '__token__'      => $this->request->param('__token__/s'),
                'login_id'       => $this->request->param('login_id/s'),
                'login_password' => $this->request->param('login_password/s'),
                'captcha'        => $this->request->param('captcha/s')
            ];
            $validate = $this->validate($data,'AdminUser.login');
            if(true !== $validate){
                return json(['code'=>0,'msg'=>$validate]);
            }
            $result  = ConfigUser::login($data);
            if($result){
                Admin::setLoginSession($result);
                return json(['code'=>200,'msg'=>'登录成功','url' => url('system/admin.index/index')]);
            }else{
                return json(['code'=>0,'msg'=>'管理帐号登录失败']);
            }
        }else{
            return view('admin/index/login'); 
        }
    }

    /**
     * 退出
     */
    public function logout(){
        Admin::setlogoutSession(); 
        Admin::clearMiniapp();
        return redirect(url('system/admin.index/login'));
    } 
}