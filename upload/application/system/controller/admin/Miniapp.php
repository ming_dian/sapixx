<?php
/**
 * @copyright   Copyright (c) 2017 https://www.sapixx.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @author      pillar<ltmn@qq.com>
 * 应用管理
 */
namespace app\system\controller\admin;
use app\common\event\Admin;
use app\common\model\Miniapp as AppMiniapp;
use app\common\model\MemberMiniapp;
use app\common\model\MemberMiniappOrder;
use app\common\model\User;
use app\system\model\MemberBank;
use app\system\model\MemberBankBill;
use app\system\event\AppConfig;
use app\system\event\Install;
use think\facade\Config;
use think\facade\Request;
use think\facade\Validate;
use think\Db;
use Exception;

class Miniapp extends Common{

    public function initialize(){
        parent::initialize();
        $this->assign('pathMaps', [['name'=>'应用管理','url'=>url("system/admin.miniapp/index")]]);
    }

    /**
     * 应用列表
     * @access public
     */
    public function index(){
        $view['list'] = AppMiniapp::order('id desc')->paginate(10);
        //查询已安装程序
        $dir          = AppMiniapp::column('miniapp_dir');
        //返回差集
        $install_app = array_diff(Install::getDir(PATH_APP,['system','common','install']),$dir);
        $i = 0;
        $app = [];
        foreach ($install_app as $value) {
            $app[$i] = AppConfig::version($value);
            $app[$i]['miniapp_dir'] = $value;
            $i++;
        }
        $view['diff'] = $app;
        return view()->assign($view);
    }

    /**
     * @param $dir
     * @return \think\response\Json
     * 安装程序
     */
    public function install(){
        try {
            $dir = $this->request->param('dir');
            if(empty($dir)){
                return json(['code' => 0, 'msg' => '未找到应用']);
            }
            $param = AppConfig::version($dir);
            if(empty($param)){
                return json(['code' => 0, 'msg' => '未找到应用配置']);
            }
            $app = AppMiniapp::column('miniapp_dir');
            if(in_array($dir,$app)){
                return json(['code' => 0, 'msg' => '应用已安装,禁止重复安装']);
            }
            //插入一条数据
            $data = [
                'types'         => $param['types'],
                'title'         => $param['name'],
                'version'       => $param['version'],
                'is_manage'     => $param['is_manage'],
                'is_wechat_pay' => $param['is_wechat_pay'],
                'is_alipay_pay' => $param['is_alipay_pay'],
                'is_openapp'    => $param['is_openapp'],
                'describe'      => $param['describe'],
                'view_pic'      => Request::root(true)."/static/{$dir}/logo.png",
                'style_pic'     => [Request::root(true)."/static/{$dir}/logo.png"],
                'content'       => $param['describe'],
                'expire_day'    => 0,
                'sell_price'    => 0,
                'market_price'  => 0,
                'miniapp_dir'   => $dir,
                'template_id'   => 0,
                'qrcode'        => ''
            ];
            $validate = $this->validate($data,'miniapp.add');
            if(true !== $validate){
                return json(['code'=>0,'msg'=>$validate]);
            }
            $file = PATH_APP.$dir.'/install.sql';
            if (file_exists($file)) {
                $array = Install::get_sql_array($file,Config::get('database.prefix'));
                foreach ($array as $sql) {
                    Db::query($sql);
                }
                AppMiniapp::edit($data);
                return json(['code' => 200, 'msg' => '操作成功']);
            }else{
                return json(['code' => 0, 'msg' => '未找到数据库脚本']);
            }
        } catch (Exception $e) {
            return json(['code' => 0,'msg' =>$e->getMessage()]);
        }
    }

        /**
     * 用户列表
     */
    public function select(){
        $keyword = $this->request->param('keyword');
        if(!empty($keyword)){
            $condition['phone_id'] = $keyword;     
        }else{
            $condition = [];
        }
        $view['keyword'] = $this->request->param('keyword');
        $view['input']   = $this->request->param('input');
        $view['list']    = AppMiniapp::where($condition)->order('id desc')->paginate(10);
        return view()->assign($view);   
    } 
    
    /**
     * 切换应用管理
     * */
    public function manage(){
        $info = AppMiniapp::where(['id' => $this->request->param('id/d')])->find();
        if(!$info){
            return json(['code'=>0,'msg'=>'操作失败']);
        }
        if($info['is_manage'] == 0){
            return json(['code'=>0,'msg'=>'当前应用没有独立管理中心']);
        }
        Admin::clearMiniapp();
        Admin::setMiniapp($info->id);
        return json(['code'=>200,'msg'=>'操作成功','url'=>url('system/admin.index/index')]);
    }

     /**
     * 列表
     * @access public
     */
    public function add(){
        if(request()->isAjax()){
            $data = [
                'types'         => $this->request->param('types/s'),
                'title'         => $this->request->param('title/s'),
                'view_pic'      => $this->request->param('view_pic/s'),
                'style_pic'     => $this->request->param('imgs/a'),
                'version'       => $this->request->param('version/s'),
                'expire_day'    => $this->request->param('expire_day/d'),
                'sell_price'    => $this->request->param('sell_price/f'),
                'market_price'  => $this->request->param('market_price/f'),
                'is_manage'     => $this->request->param('is_manage/d'),
                'is_wechat_pay' => $this->request->param('is_wechat_pay/d'),
                'is_alipay_pay' => $this->request->param('is_alipay_pay/d'),
                'miniapp_dir'   => $this->request->param('miniapp_dir/s'),
                'is_openapp'    => $this->request->param('is_openapp/d'),
                'template_id'   => $this->request->param('template_id/d'),
                'describe'      => $this->request->param('describe/s'),
                'qrcode'        => $this->request->param('qrcode/s'),
                'content'       => $this->request->param('content/s')
            ];
            $validate = $this->validate($data,'miniapp.add');
            if(true !== $validate){
                return json(['code'=>0,'msg'=>$validate]);
            }
            $result  = AppMiniapp::edit($data);
            if(!$result){
                return json(['code'=>0,'msg'=>'操作失败']);
            }else{
                return json(['code'=>200,'msg'=>'操作成功','url'=>url('system/admin.miniapp/index')]);
            }
        }else{
            return view();   
        }
    }   

    /**
     * 编辑用户
     */
    public function edit(){
        if(request()->isAjax()){
            $data = [
                'id'            => $this->request->param('id/s'),
                'types'         => $this->request->param('types/s'),
                'title'         => $this->request->param('title/s'),
                'view_pic'      => $this->request->param('view_pic/s'),
                'style_pic'     => $this->request->param('imgs/a'),
                'version'       => $this->request->param('version/s'),
                'expire_day'    => $this->request->param('expire_day/d'),
                'sell_price'    => $this->request->param('sell_price/f'),
                'market_price'  => $this->request->param('market_price/f'),
                'is_manage'     => $this->request->param('is_manage/d'),
                'is_wechat_pay' => $this->request->param('is_wechat_pay/d'),
                'is_alipay_pay' => $this->request->param('is_alipay_pay/d'),
                'miniapp_dir'   => $this->request->param('miniapp_dir/s'),
                'is_openapp'    => $this->request->param('is_openapp/d'),
                'template_id'   => $this->request->param('template_id/d'),
                'describe'      => $this->request->param('describe/s'),
                'qrcode'        => $this->request->param('qrcode/s'),
                'content'       => $this->request->param('content/s')
            ];
            $validate = $this->validate($data,'miniapp.edit');
            if(true !== $validate){
                return json(['code'=>0,'msg'=>$validate]);
            }
            $result  = AppMiniapp::edit($data);
            if(!$result){
                return json(['code'=>0,'msg'=>'操作失败']);
            }else{
                return json(['code'=>200,'msg'=>'操作成功','url'=>url('system/admin.miniapp/index')]);
            }
        }else{
            $id   = $this->request->param('id/d');
            $info = AppMiniapp::where(['id' => $id])->find();
            if(!$info){
                return $this->error("404 NOT FOUND");
            }
            $info['style_pic'] = empty($info['style_pic']) ? json_decode('[]',true):json_decode($info['style_pic'],true);
            $view['info']      = $info;
            return view()->assign($view);
        }
    }

    
    /**
     * 锁定
     * @param integer $id 用户ID
     */
    public function islock(int $id){
        $result = AppMiniapp::lock($id);
        if(!$result){
            return json(['code'=>0,'message'=>'操作失败']);
        }else{
            return json(['code'=>200,'message'=>'操作成功']);
        }
    }

    /**
     * [删除]
     * @access public
     * @return bool
     */
    public function delete(int $id){
        $member = MemberMiniapp::where(['miniapp_id' => $id])->count();
        if($member){
            return json(['code' => 0,'msg'=>'用户已开通当前应用,建议禁用']);
        }
        $result  = AppMiniapp::destroy($id);
        if(!$result){
            return json(['code' => 0,'msg'=>'操作失败']);
        }else{
            return json(['code' =>200,'msg'=>'操作成功']);
        }
    }

     /**
     * 授权管理
     * @access public
     * @return bool
     */
    public function authorizar(int $types = 0,int $miniapp_id = 0){
        $keyword = $this->request->param('keyword');
        $types = $types ? 1 : 0;
        $condition   = [];
        $condition[] = ['member_miniapp.is_lock','=',$types];
        if(!empty($keyword)){
            $data['keyword'] = $keyword;
            $is_mobile = Validate::make()->rule('keyword', 'mobile')->check($data);
            if($is_mobile){
                $condition[] = ['member.phone_id','=',$keyword];   
            }else{
                $condition[] = ['member_miniapp.appname','like','%'.$keyword.'%'];
            }
        }
        if($miniapp_id){
            $condition[] = ['member_miniapp.miniapp_id','=',$miniapp_id];  
        }
        $view['keyword'] = $this->request->param('keyword');
        $view['list']    = MemberMiniapp::lists($condition,['types' => $types,'miniapp_id' => $miniapp_id]);
        $view['miniapp_num']  = AppMiniapp::where(['is_lock' => 0])->count();
        $view['member_miniapp_num']   = MemberMiniapp::where(['is_lock' => 0])->count();
        $view['consume'] = MemberBankBill::where(['state' => 1])->sum('money');
        $view['money']   = MemberBank::sum('money');
        $view['consume'] = MemberBankBill::where(['state' => 1])->sum('money');
        $view['types']      = $types;
        $view['miniapp_id'] = $miniapp_id;
        return view()->assign($view);
    }   

     /**
     * 添加授权
     * @access public
     */
    public function addAuthorizar(){
        if(request()->isAjax()){
            $data = [
                'member_id'  => $this->request->param('member_id/d'),
                'miniapp_id' => $this->request->param('miniapp_id/d'),
                'appname'    => $this->request->param('appname/s'),
            ];
            $validate = $this->validate($data,'miniapp.addAuthorizar');
            if(true !== $validate){
                return json(['code'=>0,'msg'=>$validate]);
            }
            //新增购买列表
            $miniapp  = AppMiniapp::where(['id' => $data['miniapp_id']])->find();
            $order['member_id']  = $data['member_id'];
            $order['miniapp_id'] = $data['miniapp_id'];
            $order['start_time'] = time();
            $order['end_time']   = time()+31536000;
            $order['is_lock']    = 1;
            $data['miniapp_order_id'] = MemberMiniappOrder::insertGetId($order);
            if($data['miniapp_order_id']){
                $result  = MemberMiniapp::edit($data);
                if($result){
                    return json(['code'=>200,'msg'=>'操作成功','url'=>url('system/admin.miniapp/Authorizar')]);
                }
            } 
            return json(['code'=>0,'msg'=>'操作失败']);      
        }else{
            return view();   
        }
    }   

    /**
     * 编辑授权
     */
    public function editAuthorizar(){
        if(request()->isAjax()){
            $data = [
                'id'           => $this->request->param('id/d'),
                'uid'          => $this->request->param('uid/d'),
                'navbar_color' => $this->request->param('navbar_color/s','','filter\Filter::filter_escape'),
                'navbar_style' => $this->request->param('navbar_style/s','','filter\Filter::filter_escape'),
            ];
            $validate = $this->validate($data,'miniapp.editAuthorizar');
            if(true !== $validate){
                return json(['code'=>0,'msg'=>$validate]);
            }
            $result  = MemberMiniapp::where(['id'=>$data['id']])->update($data);
            if(!$result){
                return json(['code'=>0,'msg'=>'操作失败']);
            }else{
                return json(['code'=>200,'msg'=>'操作成功','url'=>url('system/admin.miniapp/editAuthorizar',['id' => $data['id']])]);
            }
        }else{
            $id   = $this->request->param('id/d');
            $info = MemberMiniapp::where(['id' => $id])->find();
            if(!$info){
                return $this->error("404 NOT FOUND");
            }
            $view['info'] = $info;
            $view['user'] = User::where(['id' => $info->uid])->find();
            $view['miniapp'] = AppMiniapp::where(['id' => $info->miniapp_id])->find();
            return view()->assign($view);
        }
    }

    
    /**
     * 锁定授权
     * @param integer $id 用户ID
     */
    public function islockAuthorizar(int $id){
        $result = MemberMiniapp::lock($id);
        if($result){
            return enjson(200);
        }
        return enjson(0,'解锁失败,请先解除用户锁定。');
    }

    /**
     * 读取应用所属微信(属于前台管理员)
     * @return void
     */
    public function selectWechatUser(){
        $keyword = $this->request->param('keyword');
        $view['keyword'] = $this->request->param('keyword');
        $view['input']   = $this->request->param('input');
        $view['id']      = $this->request->param('id');
        $view['list']    = [];
        if(!empty($keyword)){
            $view['list'] = User::where(['member_miniapp_id' => $view['id']])->whereLike('nickname','%'.$keyword.'%')->order('id desc')->limit(10)->select();
        }        
        return view()->assign($view);
    }
}