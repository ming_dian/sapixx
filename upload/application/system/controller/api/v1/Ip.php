<?php
/**
 * @copyright   Copyright (c) 2017 https://www.sapixx.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @author      pillar<ltmn@qq.com>
 * 根据地址位置定位服务
 */
namespace app\system\controller\api\v1;
use think\Controller;
use Yurun\Util\HttpRequest;

class Ip extends Controller{

    /**
     * 根据IP地址定位位置
     * @return json
     */
    public function index(){
        $http = HttpRequest::newSession();
        $taobao = $http->get("http://ip.taobao.com/service/getIpInfo.php?ip=222.88.142.161")->jsonp(true);
        $address = null;
        if(!empty($taobao) && $taobao['code'] == 0){
            $address = $taobao['data']['region'] == $taobao['data']['city'] ? $taobao['data']['region'] : $taobao['data']['region'].$taobao['data']['city'];
        }else{
            $sina = $http->get("http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=json&ip={$ip}")->jsonp(true);
            if($sina != -2 && is_array($sina)){
                $address = $sina['province'] == $sina['city'] ? $sina['province'] : $sina['province'].$sina['city'];
            }   
        }
        if(empty($address)){
            return json(['code'=>0,'msg'=>'没有内容了']);
        }else{
            return json(['code'=>200,'msg'=>'成功','data'=>['address' => $address]]);
        }
    }
}