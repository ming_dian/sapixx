<?php
namespace app\system\controller\api\v1;
use app\common\facade\WechatPay;
use app\system\model\MemberBank;
use app\system\model\MemberBankBill;
use app\system\model\MemberBankRecharge;
use think\Controller;
use Exception;

class Notify extends Controller{

    /**
     * 服务商统一回调通知
     * @return void
     */
    public function index(){
        try {
            $response = WechatPay::doPay()->handlePaidNotify(function($message,$fail){
                $rel = MemberBankRecharge::where(['order_sn' => $message['out_trade_no'],'state' => 0])->find();
                if (empty($rel)) {
                    return true;
                }
                if ($message['result_code'] === 'SUCCESS') {
                    if($message['result_code'] === 'SUCCESS'){
                        $ispay = WechatPay::doPay()->order->queryByOutTradeNumber($rel->order_sn);
                        if ($ispay['return_code'] === 'SUCCESS') {
                            if ($ispay['result_code'] === 'SUCCESS') {
                                if ($ispay['trade_state'] === 'SUCCESS'){
                                    $memberBankBill = new MemberBankBill();
                                    if($ispay['total_fee'] == $rel->money * 100){
                                        $data['state']     = 1;
                                        $data['update_time']    = time();
                                        $data['transaction_id'] = $message['transaction_id'];
                                        MemberBankRecharge::where(['id' => $rel->id])->update($data);
                                    }
                                    $memberBankBill->money       = $ispay['total_fee']/100;
                                    $memberBankBill->member_id   = $rel->member_id;
                                    $memberBankBill->update_time = time();
                                    $memberBankBill->state = 0;
                                    $memberBankBill->save();
                                    MemberBank::moneyUpdate($rel->member_id,$ispay['total_fee']/100);
                                }
                            }
                        }
                    }
                }
                return $fail('通信失败,请稍后再通知我');
            });
            $response->send();
        }catch (Exception $e) {
            $this->error('页面不存在');
        }
    }
}