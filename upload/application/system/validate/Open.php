<?php
/**
 * @copyright   Copyright (c) 2017 https://www.sapixx.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @author      pillar<ltmn@qq.com>
 * 开发平台
 */
namespace app\system\validate;
use think\Validate;

class Open extends Validate{

    protected $rule = [
        'page'                 => 'require|array',
        'cate'                 => 'require|array',
        'name'                 => 'require|array',
        'tag'                  => 'require|array',
    ];
    
    protected $message = [
        'page'                 => '页面路径必须填写',
        'cate'                 => '页面目录必须填写',
        'name'                 => '页面名称必须填写',
        'tag'                  => '页面标签必须填写',
    ];

    protected $scene = [
        'addpass' => ['cate','page','name','tag']
    ];
}