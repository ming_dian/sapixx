<?php
/**
 * 钱和积分的表单验证器
 */
namespace app\system\validate;
use think\Validate;

class MemberBank extends Validate{

    protected $rule = [
        'id'           => 'require|integer',
        'miniapp_id'   => 'require|integer',
        'member_id'    => 'require|integer',
        'money'        => 'require|integer|gt:0',
        'safepassword' => 'require',
    ];

    protected $message = [
        'id'                 => '配置ID丢失',
        'member_id'          => '未找到对应用户',
        'money.require'      => '充值金额必须填写',
        'money.integer'      => '充值金额必须是整数',
        'money.gt'           => '充值金额必须大于0',
        'safepassword'       => '安全验证密码没有输入',
    ];

    protected $scene = [
        'buy'      => ['id','member_id','safepassword'], //购买应用
        'recharge' => ['member_id','money'], //充值
    ];
}