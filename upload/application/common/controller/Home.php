<?php
/**
 * @copyright   Copyright (c) 2018 https://www.sapixx.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @author      pillar<ltmn@qq.com>
 * 浏览器无权限访问控制基类
 */
namespace app\common\controller;

class Home extends Base{

     /**
     * 初始化类
     */
    protected function initialize(){ 
        parent::initialize();
    } 

}
