<?php
/**
 * @copyright   Copyright (c) 2018 https://www.sapixx.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @author      pillar<ltmn@qq.com>
 *  浏览器基类
 */
namespace app\common\controller;
use think\Controller;
use app\common\model\ConfigWeb;

class Base extends Controller{

    protected $web;      //站点参数

    protected function initialize(){
        $this->web      = ConfigWeb::config();  //当前站点配置
        $this->assign(['web'=> $this->web]);
    }
    
    /**
     * 方法不存在
     */
    public function _empty(){
        return $this->error('访问的页面不存在');
    }

}
